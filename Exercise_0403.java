import java.util.Scanner;

public class Exercise_0403 {
    public static void main(String[] args) {
       // DayinMonth();
       // DayinWeek();
      //  triangleNum();
       // triangleIncreased1();

    }
    private static void DayinMonth(){
        Scanner sc=new Scanner(System.in);

        int month=0;
        System.out.print("Input Month from 1 to 12:");
        month=sc.nextInt();
       while (month<0|month>12){
           System.out.print("Input wrong. Please input Month from 1 to 12:");
           month=sc.nextInt();
        }

switch (month){
    case 1:
        System.out.println("Thang  1 co 31 ngay");
        break;
    case 2:
        System.out.println("Thang  2 co 28 hoac 29 ngay");
        break;
    case 3:
        System.out.println("Thang  3 co 31 ngay");
        break;
    case 4:
        System.out.println("Thang  4 co 30 ngay");
        break;
    case 5:
        System.out.println("Thang  5 co 31 ngay");
        break;
    case 6:
        System.out.println("Thang  6 co 30 ngay");
        break;
    case 7:
        System.out.println("Thang  7 co 31 ngay");
        break;
    case 8:
        System.out.println("Thang  8 co 31 ngay");
        break;
    case 9:
        System.out.println("Thang  9 co 30 ngay");
        break;
    case 10:
        System.out.println("Thang  10 co 31 ngay");
        break;
    case 11:
        System.out.println("Thang  11 co 30 ngay");
        break;
    case 12:
        System.out.println("Thang  12 co 21 ngay");
        break;
}
    }
    private  static void DayinWeek(){
      Scanner sc1=new Scanner(System.in);
      int week=0;
        System.out.print("Input day in week: ");
        week=sc1.nextInt();
        while (week<0 | week>7)
        {
            System.out.print("Input wrong. Please input day from 1 to 7: ");
            week=sc1.nextInt();
        }
        switch (week){
            case 1:
                System.out.println("To day is Monday");
                break;
            case 2:
                System.out.println("To day is Tuesday");
                break;
            case 3:
                System.out.println("To day is Wednesday");
                break;
            case 4:
                System.out.println("To day is Thursday");
                break;
            case 5:
                System.out.println("To day is Friday");
                break;
            case 6:
                System.out.println("To day is Saturday");
                break;
            case 7:
                System.out.println("To day is Sunday");
                break;




        }

    }
    private static void triangleNum(){
        Scanner sc2=new Scanner(System.in);
        int i,j,n;
        System.out.println("Input number lines: n=");
        n= sc2.nextInt();
        while (n<0){
            System.out.println("Input wrong. Please input n=");
            n=sc2.nextInt();
        }
        for (i = 1; i <= n; i++) {
            for (j=1;j<=i;j++){
                System.out.print(j+" ");
                            }
            System.out.println("");
        }

    }
    private static void triangleIncreased1(){
        int i,j,n,k=1;
        System.out.print("Input number of rows n=: ");
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=i;j++)
                System.out.print(k++);
            System.out.println("");
        }

    }
}
