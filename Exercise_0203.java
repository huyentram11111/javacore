import java.util.Arrays;
import java.util.Scanner;

public class Exercise_0203 {
    public static void main(String[] args) {

       // findMinMax();
       // findSecLagest();
       // insertArray();
        checkDuplicate();
    }

    private static void findMinMax() {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.println("Nhap so phan tu cua mang: n=");
        n = sc.nextInt();
        int[] numberArr = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Nhap gia tri cho mang: a[" + i + "]=");
            numberArr[i] = sc.nextInt();
        }
        System.out.println("Cac phan tu cua mang la:");
        for (int i = 0; i < n; i++) {
            System.out.print(numberArr[i] + ",");
        }
        int min = numberArr[0];
        int max = numberArr[0];
        for (int i = 0; i < numberArr.length; i++) {
            if (numberArr[i] < min) {
                min = numberArr[i];
            }
            if (max < numberArr[i]) {
                max = numberArr[i];
            }
        }
        System.out.println("");
        System.out.println("So nho nhat la:" + min);
        System.out.println("So lon nhat la:" + max);

    }

    private static void findSecLagest() {

       int [] arr = {23,43,33,555,232,433};
        Arrays.sort(arr);
        System.out.println("Mang sau khi duoc sap xep la");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
        System.out.println("Phan tu lon thu 2 la:"+arr[arr.length-2]);
           }


       private static void insertArray(){
        Scanner sc = new Scanner(System.in);
        int n, k, a;
        System.out.println("Nhap do lon mang:");
        n = sc.nextInt();
        int numberArr[] = new int[n+1];
        for (int i = 0; i < n; i++) {
            System.out.println("\nNhap phan tu thu " + i + ":");
            numberArr[i] = sc.nextInt();

        }
        System.out.println("Cac phan tu trong mang la:");
        for (double i : numberArr) {
            System.out.print(i + ",");
        }
        System.out.print("Nhap vi tri muon chen phan tu vao mang:");
           k = sc.nextInt();
        System.out.print("Nhap gia tri muon chen vao mang:");
        a = sc.nextInt();
        for (int i = (n-1); i >= (k - 1); i--){
            numberArr[i+1] = numberArr[i];
        }
        numberArr[k-1] = a;
        System.out.println("Sau khi chen:");
        for (int i = 0; i < n; i++)
        {
            System.out.print(numberArr[i]+",");
        }
        System.out.print(numberArr[n]);

    }
    private static void checkDuplicate(){
        String [] str= {"Tieng anh", "Tieng phap","Tieng anh","Tieng phap"};

        for (int i = 0; i < str.length-1; i++) {
            for (int j = i+1; j < str.length; j++) {
                if (str[i].equals(str[j]) && i!=j)
                {
                    System.out.println("Phan tu bi trung la: "+str[i]);
                }
            }
        }

    }
}
