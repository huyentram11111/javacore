import java.util.Scanner;
import java.util.Formatter;

public interface BT1403_TienThueNha {
    public static void main(String[] args) {
        int sonuoc=0;
        int sodien=0;
        int tiennha=0;
        Scanner sc =new Scanner(System.in);
        System.out.print("Nhap so nuoc:");
        sonuoc=sc.nextInt();
        System.out.print("Nhap so dien");
        sodien=sc.nextInt();
        if (sodien>0 && sodien<= 50){
              tiennha=sodien*1678+sonuoc*5000+2500000;


        }
        else if (sodien>50 && sodien<=100){
            tiennha=50*1678+  (sodien-50)*1734 +sonuoc*5000+2500000;

        }
        else if (sodien>100 && sodien<=200){
            tiennha=50*1678+  100*1734 +(sodien-100)*2014 +sonuoc*5000+2500000;

        }
        else if (sodien>200 && sodien<=300){
            tiennha=50*1678+  100*1734 + 200*2014 +(sodien-200)*2536 +sonuoc*5000+2500000;

        }
        else if (sodien>300 && sodien<=400){
            tiennha=50*1678+  100*1734 + 200*2014 + 300*2536  +(sodien-300)*2536 +sonuoc*5000+2500000;

        }
        else if (sodien>400 ){
            tiennha=50*1678+  100*1734 + 200*2014 + 300*2536 + 400*2536  +(sodien-400)*2927 +sonuoc*5000+2500000;

        }
        Formatter formatter = new Formatter();
        // Use Space format specifier
        formatter.format("%,d", tiennha);
        System.out.println("Tien nha la: "+ formatter+ "VND");
    }
}


/** KET QUA:
 Nhap so nuoc:12
 Nhap so dien
 401
 Tien nha la: 4,998,227VND
 */
