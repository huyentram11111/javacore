import java.util.Scanner;

public class BT1403_240_255 {
    public static void main(String[] args) {
        System.out.print("Nhap so phan tu mang: n=");
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
    // BT 240: Kiem tra gia 0 trong mot mang

     int x=returnNum(n);
        System.out.println("Bai 240: tim phan tu co gia tri 0");
     if (x==0)
     {
         System.out.println("Mang chua phan tu 0");
     }
     else {
         System.out.println("Mang khong chua phan tu 0");
     }

     //BT 255: Sap xep mang tang dan
        System.out.println("Bai 255: Sap xep mang theo thu tang dan");
        int[] arrNum=returnArr(n);
        int temp=0;
        for (int i = 0; i < arrNum.length-1; i++) {
            for (int j = i+1; j <= arrNum.length-1; j++) {
                if (arrNum[i]>arrNum[j])
                {
                    temp=arrNum[i];
                    arrNum[i]=arrNum[j];
                    arrNum[j]=temp;
                }
            }

        }
        System.out.println("Mang sau khi sap xep theo thu tu tang dan la:");
        for (int i = 0; i < arrNum.length; i++) {
            System.out.print(arrNum[i]+" ");
        }

    }
    public static int[] returnArr(int n){

        int i,x=0;
        Scanner sc=new Scanner(System.in);
        int[] arrList =new int[n];
        for (i = 0; i < n; i++) {
            System.out.print("Phan tu thu "+i+" la: ");
            arrList[i]=sc.nextInt();
        }
        return arrList;

    }
    public  static int returnNum(int n){
    int i,x=0;
    int [] arr=returnArr(n);
    for (i=0;i<arr.length;i++)
        if (arr[i]==0)
        {
            x=0;
        }
    else {
        x=1;
        }
    return x;
    }
}


/*=========
Ket qua chạy:
Mang khong chua phan tu 0
        Bai 255: Sap xep mang theo thu tang dan
        Phan tu thu 0 la: 44
        Phan tu thu 1 la: 23
        Phan tu thu 2 la: 1
        Mang sau khi sap xep theo thu tu tang dan la:
        1 23 44
 */